// Generated on 2014-01-03 using generator-webapp-rjs 0.4.4
'use strict';

var LIVERELOAD_PORT = 35729;

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        // configurable paths
        yeoman: {
            _public: 'public',
            assets: 'app/assets'
        },
        watch: {
            coffee: {
                files: ['<%= yeoman.assets %>/coffee/**/*.coffee'],
                tasks: ['coffee:dist']
            },
            handlebars: {
                files: ['<%= yeoman.assets %>/coffee/**/*.hbs'],
                tasks: ['handlebars']
            },
            compass: {
                files: ['<%= yeoman.assets %>/sass/{,*/}*.{scss,sass}'],
                tasks: ['compass:server']
            },
            livereload: {
                options: {
                    livereload: LIVERELOAD_PORT
                },
                files: [
                    '<%= yeoman._public %>/css/{,*/}*.css',
                    '<%= yeoman._public %>/js/{,*/}*.js',
                    '<%= yeoman._public %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        // clean: {
        //     dist: {
        //         files: [{
        //             dot: true,
        //             src: [
        //                 '.tmp',
        //                 '<%= yeoman.dist %>/*',
        //                 '!<%= yeoman.dist %>/.git*'
        //             ]
        //         }]
        //     },
        //     server: '.tmp'
        // },
        coffee: {
            options: {
                bare: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.assets %>/coffee',
                    src: '**/*.coffee',
                    dest: '<%= yeoman._public %>/js',
                    ext: '.js'
                }]
            }
        },
        handlebars: {
            dist: {
                options: {
                    amd: true,
                    processName: function(filename) {
                        var regex = /app\/assets\/coffee\/backbone\/(apps\/)?/gi;
                        var name = filename.replace(regex, '');

                        return name.replace('templates/', '').slice(0, -4);
                    }
                },
                files: {
                    '<%= yeoman._public %>/js/templates/templates.js': '<%= yeoman.assets %>/coffee/**/*.hbs'
                }
            }
        },
        compass: {
            options: {
                sassDir: '<%= yeoman.assets %>/sass',
                cssDir: '<%= yeoman._public %>/css',
                generatedImagesDir: '<%= yeoman._public %>/images',
                imagesDir: '<%= yeoman.assets %>/sass/images',
                javascriptsDir: '<%= yeoman._public %>/js',
                fontsDir: '<%= yeoman._public %>/css/fonts',
                importPath: '<%= yeoman._public %>/bower_components',
                httpImagesPath: '/images',
                httpGeneratedImagesPath: '/images',
                httpFontsPath: '/css/fonts',
                relativeAssets: false,
                assetCacheBuster: false
            },
            server: {
                options: {
                    debugInfo: false,
                    noLineComments: true
                }
            }
        },
        // not used since Uglify task does concat,
        // but still available if needed
        /*concat: {
            dist: {}
        },*/
        requirejs: {
            dist: {
                // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
                options: {
                    // `name` and `out` is set by grunt-usemin
                    mainConfigFile: '<%= yeoman._public %>/js/main.js',
                    baseUrl: '<%= yeoman._public %>/js',
                    name: 'main',
                    out: '<%= yeoman._public %>/js/main.js',
                    optimize: 'uglify2',
                    // TODO: Figure out how to make sourcemaps work with grunt-usemin
                    // https://github.com/yeoman/grunt-usemin/issues/30
                    //generateSourceMaps: true,
                    // required to support SourceMaps
                    // http://requirejs.org/docs/errors.html#sourcemapcomments
                    preserveLicenseComments: false,
                    useStrict: true,
                    wrap: false
                    //uglify2: {} // https://github.com/mishoo/UglifyJS2
                }
            }
        },
        // imagemin: {
        //     dist: {
        //         files: [{
        //             expand: true,
        //             cwd: '<%= yeoman.app %>/images',
        //             src: '{,*/}*.{png,jpg,jpeg}',
        //             dest: '<%= yeoman.dist %>/images'
        //         }]
        //     }
        // },
        // svgmin: {
        //     dist: {
        //         files: [{
        //             expand: true,
        //             cwd: '<%= yeoman.app %>/images',
        //             src: '{,*/}*.svg',
        //             dest: '<%= yeoman.dist %>/images'
        //         }]
        //     }
        // },
        cssmin: {
            dist: {
                files: {
                    '<%= yeoman._public %>/css/main.css': ['<%= yeoman._public %>/css/main.css']
                }
            }
        },
        // Put files not handled in other tasks here
        // copy: {
        //     dist: {
        //         files: [{
        //             expand: true,
        //             dot: true,
        //             cwd: '<%= yeoman.app %>',
        //             dest: '<%= yeoman.dist %>',
        //             src: [
        //                 '*.{ico,png,txt}',
        //                 '.htaccess',
        //                 'images/{,*/}*.{webp,gif}',
        //                 'styles/fonts/{,*/}*.*',
        //                 'bower_components/sass-bootstrap/fonts/*.*'
        //             ]
        //         }]
        //     }
        // },
        concurrent: {
            server: [
                'coffee:dist',
                'handlebars',
                'compass'
            ],
            dist: [
                'coffee',
                'handlebars',
                'compass'
                // 'imagemin',
                // 'svgmin'
            ]
        },
        bower: {
            target: {
                rjsConfig: '<%= yeoman._public %>/js/main.js'
            }
        }
    });

    grunt.registerTask('server', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build']);
        }

        grunt.task.run([
            // 'clean:server',
            'concurrent:server',
            'watch'
        ]);
    });

    grunt.registerTask('build', [
        // 'clean:dist',
        'concurrent:dist',
        'requirejs',
        // 'concat',
        'cssmin'
        // 'uglify'
    ]);

    grunt.registerTask('default', [
        'bower',
        'build'
    ]);
};
