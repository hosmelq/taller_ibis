<?php

class WorkshopsTableSeeder extends Seeder {

	public function run()
	{
        $faker = Faker\Factory::create();

		// Uncomment the below to wipe the table clean before populating
		DB::table('workshops')->truncate();

        $workshops = [
            [
                'title' => 'WorkFlow',
                'user_id' => 1,
                'location' => 'IBIS Servicios',
                'status' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
        ];

        DB::table('workshops')->insert($workshops);
	}

}
