<?php

class AttachmentsTableSeeder extends Seeder {

	public function run()
	{
        $faker = Faker\Factory::create();

		// Uncomment the below to wipe the table clean before populating
		DB::table('attachments')->truncate();

        foreach (range(1, 40) as $key) {
            Attachment::create([
                'workshop_id' => rand(1, 20),
                'name' => $faker->name,
                'path' => $faker->address,
            ]);
        }
	}

}
