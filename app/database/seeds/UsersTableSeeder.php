<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
        $faker = Faker\Factory::create();

		// Uncomment the below to wipe the table clean before populating
		DB::table('users')->truncate();

        $users = [
            [
                'fullname' => 'Hosmel Quintana',
                'email' => 'hosmelq@gmail.com',
                'password' => Hash::make('jade100'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Carlos Saldaña',
                'email' => 'carl.sald19@gmail.com',
                'password' => Hash::make('carlos'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Laoska Benyasca',
                'email' => 'lbgm2011@gmail.com',
                'password' => Hash::make('laoska'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Cristhian Vallecillo',
                'email' => 'cristhianjvl29@gmail.com',
                'password' => Hash::make('cristhian'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Fernando Larios',
                'email' => 'lariosly2@gmail.com',
                'password' => Hash::make('fernando'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Virginia Arellano',
                'email' => 'virginia0350@gmail.com',
                'password' => Hash::make('virginia'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Anthony Webster',
                'email' => 'anthonyanroab@gmail.com',
                'password' => Hash::make('anthony'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Franco Salas',
                'email' => 'ing.francosalasmedina@outlook.com',
                'password' => Hash::make('franco'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Frederick Lozano',
                'email' => 'apocalipsis201@gmail.com',
                'password' => Hash::make('frederick'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'River Martinez',
                'email' => 'rianmartinez@gmail.com',
                'password' => Hash::make('river'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'fullname' => 'Gaby',
                'email' => 'gshareck.03@gmail.com',
                'password' => Hash::make('gaby'),
                'active' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
        ];

        DB::table('users')->insert($users);
	}

}
