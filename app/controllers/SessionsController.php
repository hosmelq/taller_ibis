<?php

use Acme\Services\Validation\ValidationException;

class SessionsController extends BaseController {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('sessions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        try
        {
            App::make("Acme\\Services\\Validation\\SessionValidator")->validateForCreation(Input::all());

            $attempt = Auth::attempt([
                'email' => Input::get('email'),
                'password' => Input::get('password'),
                'active' => 1
            ]);

            if ($attempt) return Redirect::intended('/');

            return Redirect::back()->withInput()->with('flash_message', trans('app.login_required_fileds'));
        }

        catch (ValidationException $e)
        {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }
	}

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
        Auth::logout();

        return Redirect::to('login');
    }

    /**
     * pusher channel auth endpoint
     *
     * @param  int  $id
     * @return Response
     */
    public function pusherAuth()
    {
        $user = Auth::user();
        $channel_name = Input::get('channel_name');
        $socket_id = Input::get('socket_id');
        $channel_type = explode('-', $channel_name);

        if ($channel_type[0] === 'private') {
            echo Pusherer::socket_auth($channel_name, $socket_id);
        } elseif ($channel_type[0] === 'presence') {
            echo Pusherer::presence_auth($channel_name, $socket_id, $user->id, $user->toArray());
        }
    }

}
