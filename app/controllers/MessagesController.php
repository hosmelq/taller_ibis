<?php

class MessagesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($workshopId)
	{
        return Message::where('workshop_id', $workshopId)->get();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();
        $channel = 'presence-' . App::environment() . '-workshop-' . $input['workshop_id'];

        $message = Message::create($input);

        if (!$message) {
        }

        Pusherer::trigger($channel, 'messages-update', $message->toArray(), $input['socket_id']);

        return $message;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function show($workshopId, $messageId)
	{
        return View::make('messages.show');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($workshopId, $messageId)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($workshopId, $messageId)
	{
		//
	}

}
