<?php

Route::get('static', function ()
{
    return View::make('static');
});


# Sessions routes

Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@create']);
Route::get('logout', ['as' => 'logout', 'uses' => 'SessionsController@destroy']);
Route::resource('sessions', 'SessionsController', ['only' => ['create', 'store', 'destroy']]);


# api routes

Route::group(['before' => 'auth|csrf', 'prefix' => 'api/v1'], function ()
{
    # pusher channel auth endpoint
    Route::post('pusher/auth', 'SessionsController@pusherAuth');

    # workshops
    Route::resource('workshops', 'WorkshopsController', ['only' => ['index', 'show']]);

    # messages
    Route::resource('messages', 'MessagesController', ['only' => ['store']]);

    # nested resource

    # workshops files
    Route::resource('workshops.files', 'AttachmentsController', ['only' => ['index']]);

    # workshops messages
    Route::resource('workshops.messages', 'MessagesController', ['only' => ['index']]);

});


# backbone routes

Route::get('/{path?}', function()
{
    $user = Auth::user();
    $user->avatar = 'http://www.gravatar.com/avatar/' . md5($user->email) . '?d=mm&s=40';
    $locale = require app_path() . '/lang/' . App::getLocale() . '/app.php';

    return View::make('app', compact('locale', 'user'));
})->where('path', '.*')->before('auth');
