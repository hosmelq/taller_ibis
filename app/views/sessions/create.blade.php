@extends('layouts.master')

@section('title')
Login | Talleres...
@stop

@section('container')
    {{ Form::open(['accept-charset' => 'UTF-8', 'route' => 'sessions.store', 'class' => 'main-login']) }}
        @if (Session::has('flash_message'))
        <p class="main-login__error main-login__error--center">{{ Session::get('flash_message') }}</p>
        @endif

        <dl>
            <dt>
                {{ Form::label('email', trans('app.login_email'), ['class' => 'control-label']) }}
            </dt>
            <dd>
                {{ Form::email('email', null, ['tabindex' => 1, 'requiredd', 'class' => 'form-control', 'autofocus']) }}
                {{ $errors->first('email', '<span class="main-login__error">:message</span>') }}
            </dd>
            <dt>
                {{ Form::label('password', trans('app.login_password'), ['class' => 'control-label']) }}
            </dt>
            <dd>
                {{ Form::password('password', ['tabindex' => 2, 'requiredd', 'class' => 'form-control']) }}
                {{ $errors->first('password', '<span class="main-login__error">:message</span>') }}
            </dd>
        </dl>

        <div class="login-box-footer">
            {{ Form::submit(trans('app.login_sign_in'), ['tabindex' => 3, 'class' => 'btn btn-primary btn-block']) }}
        </div>
    {{ Form::close() }}
@stop
