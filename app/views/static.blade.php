<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Talleres...</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    </head>

    <body>
        <div class="page-wrap">
            <nav class="global-nav">
                <div class="global-nav__header">
                    <div class="page-width">
                        <div class="global-nav__header-top">
                            <div class="user-links">
                                <div class="user-links__greeting">
                                    <span class="user-links__greeting-name">Heyy, Hosmel</span>
                                </div>
                                <div class="user-links__separator"> | </div>
                                <a href="#" class="user-links__sign-out-link ">Sign Out</a>
                            </div>
                        </div>
                        <div class="global-nav__content">
                            <ul class="global-nav__header-list">
                                <li class="global-nav__header-item">
                                    <a href="#" class="global-nav__header-link">Courses</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <main id="main-region" class="content content--app">
                <div class="page-width">
                    <div class="grid main">
                        <div class="grid-column-3-4">
                            <div class="app-view-main app">
                                <div class="grid-tile app-wrapper">
                                    <div class="app__title-and-meta">
                                        <h1 class="app_title">List Courses</h1>
                                        <div class="app__meta">
                                            <div class="app__items-count">10 Courses</div>
                                        </div>
                                    </div>
                                    <div class="items-list">
                                        <table class="items-list--table table table-bordered table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Speaker</th>
                                                    <th>Location</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="#">WorkFlow</a></td>
                                                    <td>Hosmel Quintana</td>
                                                    <td>IBIS Servicios</td>
                                                    <td>Pending</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-column-1-4">
                            <div class="app-view-sidebar">
                                <div class="grid-tile">
                                    <h1>Sidebar</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </body>
</html>
