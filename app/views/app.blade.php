@extends('layouts.master')

@section('container')
    <div id="header-region"></div>
    <main id="main-region" class="page-width content content--app"></main>
@stop

@section('footer.js')
    <script>
    var Taller = {};
    Taller.auth_token = '{{ csrf_token() }}';
    Taller.environment = '{{ App::environment() }}';
    Taller.api_endpoint = '/api/v1';
    Taller.pusher_key = '{{ Config::get('pusherer::key'); }}';
    Taller.SIGNALING_SERVER = 'http://198.199.106.186:9999/';
    Taller.user = {{ $user }};
    Taller.Locale = { @foreach($locale as $key => $item) {{ $key }}: "{{ $item }}", @endforeach };
    Taller.config = {
        iceservers: [{"url":"turn:homeo@turn.bistri.com:80","credential":"homeo"},{"url":"stun:stun.l.google.com:19302"}]
    }
    </script>

    <script data-main="{{ asset('js/main') }}" src="{{ asset('bower_components/requirejs/require.js') }}"></script>
@stop
