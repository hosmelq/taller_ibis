<?php namespace Acme\Services\Validation;

class SessionValidator extends Validator {

    /**
     * Default rules
     *
     * @var array
     */
    protected $rules = [
        'email' => 'required|email',
        'password' => 'required'
    ];
}
