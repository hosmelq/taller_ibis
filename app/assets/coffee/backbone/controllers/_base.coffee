define [
    'app'
],

(App) ->

    App.module "Controllers", (Controllers, App, Backbone, Marionette, $, _) ->
	
        class Controllers.Base extends Marionette.Controller

            constructor: (options = {}) ->
                @region = options.region or App.request 'default:region'
                @_instance_id = _.uniqueId 'controller'
                App.execute 'register:instance', @, @_instance_id
                super

            close: ->
                App.execute 'unregister:instance', @, @_instance_id
                super

            show: (view, options = {}) ->
                _.defaults options,
                    loading: false
                    region: @region

                @_setMainView view
                @_manageView view, options

            _setMainView: (view) ->
                return if @_mainView
                @_mainView = view
                @listenTo view, 'close', @close

            _manageView: (view, options) ->
                if options.loading
                    # show loading view
                    App.execute 'show:loading', view, options

                else
                    options.region.show view