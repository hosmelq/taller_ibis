define [
    "rtcmulticonnection"
    "socketio"
],

(RTCMultiConnection) ->
    'use strict'

    RTCMultiConnection::openSignalingChannel = (config) ->
        channel = config.channel || this.channel || 'default-namespace'
        sender = Math.round(Math.random() * 9999999999) + 9999999999

        io.connect(Taller.SIGNALING_SERVER).emit 'new-channel',
            channel: channel
            sender: sender

        socket = io.connect(Taller.SIGNALING_SERVER + channel)
        socket.channel = channel

        socket.on 'connect', -> if config.callback then config.callback(socket)

        socket.send = (message) ->
            socket.emit 'message',
                data: message
                sender: sender

        socket.on('message', config.onmessage)
