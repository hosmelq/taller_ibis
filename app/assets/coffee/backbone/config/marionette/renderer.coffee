define [
    'marionette'
],

(Marionette) ->

    _.extend Marionette.Renderer,

        # Re-definir la función 'render' para usar templates pre-compiladas

        render: (template, data) ->
            return if template is false
            path = @getTemplate(template)
            throw "Template #{template} not found!" unless path
            return path(data)

        getTemplate: (template) ->
            JST[template]
