define [
    "jquery"
],

($) ->

    # agregar opciones a todas las peticiones ajax

    $.ajaxPrefilter (options, originalOptions, jqXHR) ->
        options.dataType = "jsonp"

        options.url = Taller.host + Taller.api_endpoint + options.url

        jqXHR.setRequestHeader "X-CSRF-Token", Taller.auth_token

        @
