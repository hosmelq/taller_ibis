define [
    "jquery"
    "screenfull"
],

($) ->

    $.fn.screenfull = (obj = {}) ->
        _.defaults obj,
            action: "request"

        method = obj["action"]

        throw "Method #{method} not found!" unless _.isFunction screenfull[method]

        if screenfull.enabled then screenfull[method] @[0] else console.warn "Not Fullscreen API"
