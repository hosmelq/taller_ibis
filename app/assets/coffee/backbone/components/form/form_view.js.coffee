define [
    "app"
    "bb/views/_base/layout"
],

(App) ->

    App.module 'Components.Form', (Form, App, Backbone, Marionette, $, _) ->

        class Form.FormWrapper extends App.Views.Layout
            tagName: "form"
            template: "components/form/form"

            attributes: ->
                "data-type": @getFormDataType()

            regions:
                formContentRegion: "#form-content-region"

            ui:
                buttonContainer: "ul.inline-list"

            triggers:
                "submit": "form:submit"
                "click [data-form-button='cancel']": "form:cancel"

            modelEvents:
                "change:_errors": "changeErrors"

            initialize: ->
                @getButtonTitle()
                @setInstancePropertiesFor "config"

            onShow: ->
                _.defer =>
                    @focusFirstInput() if @config.focusFirstInput

            focusFirstInput: ->
                @$(":input:visible:enabled:first").focus()

            getFormDataType: ->
                if @model.isNew() then "new" else "edit"

            changeErrors: (model, errors, options) ->
                if @config.errors
                    if _.isEmpty(errors) then @removeErrors() else @addErrors errors

            removeErrors: ->
                @$(".has-error").removeClass("has-error").find("span").remove()

            addErrors: (errors = {}) ->
                @addError name, array[0] for name, array of errors

            addError: (name, error) ->
                el = @$("[name='#{name}']")
                sp = $("<span>").text(error).addClass("help-block")
                el.after(sp).closest(".form-group").addClass "has-error"
    
            setInstancePropertiesFor: (args...) ->
                for key, val of _.pick(@options, args...)
                    @[key] = val

            getButtonTitle: ->
                @model.set "_buttons": text: if @model.isNew() then "Crear" else "Actualizar"