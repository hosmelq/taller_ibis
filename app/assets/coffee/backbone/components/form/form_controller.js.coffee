define [
    "app"
    "bb/entities/_base/_utilities"
    "bb/controllers/_base"
    "backbone.syphon"
],

(App) ->

    App.module 'Components.Form', (Form, App, Backbone, Marionette, $, _) ->

        class Form.Controller extends App.Controllers.Base

            initialize: (options = {}) ->
                @contentView = options.view
                @formLayout = @getFormLayout options.config

                @listenTo @formLayout, "show", @formContentRegion
                @listenTo @formLayout, "form:cancel", @formCancel
                @listenTo @formLayout, "form:submit", @formSubmit

            formCancel: ->
                @contentView.triggerMethod "form:cancel"

            formSubmit: ->
                data = Backbone.Syphon.serialize @formLayout

                if @contentView.triggerMethod("form:submit", data) isnt false
                    model = @contentView.model
                    collection = @contentView.collection
                    @processFormSubmit data, model, collection

            processFormSubmit: (data, model, collection) ->
                model.save data,
                    collection: collection

            formContentRegion: ->
                @region = @formLayout.formContentRegion
                @show @contentView

            getFormLayout: (options = {}) ->
                config = @getDefaultConfig _.result(@contentView, "form")
                _.extend config, options

                new Form.FormWrapper
                    config: config
                    model: @contentView.model

            getDefaultConfig: (config = {}) ->
                _.defaults config,
                    focusFirstInput: true
                    errors: true
                    syncing: true

        App.reqres.setHandler "form:wrapper", (contentView, options = {}) ->
            throw new Error "No model found inside of form's contentView" unless contentView.model

            formController = new Form.Controller
                view: contentView
                config: options
            formController.formLayout