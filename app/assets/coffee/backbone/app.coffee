define [
    "marionette"
    "bb/config/javascript"
    "bb/config/backbone/sync"
    "bb/config/marionette/application"
    "bb/config/marionette/renderer"
    "bb/config/jquery/ajax_prefilter"
    "bb/config/rtcmulticonnection/opensignalingchannel"
    "pusher"
],

(Marionette) ->
    "use strict"

    App = new Marionette.Application()

    App.on "initialize:before", (options) ->
        @environment = options.environment
        @currentUser = App.request "set:current:user", options.currentUser

    App.addRegions
        headerRegion: "#header-region"
        mainRegion: "#main-region"

    App.rootRoute = "workshops"

    # Inicializar modulos principales

    App.addInitializer ->
        App.module('HeaderApp').start()

    # configurar host

    App.addInitializer ->
        Taller.host = if App.environment is "production" then "https://ibis.hosmelquintana.com" else "https://staller.dev"

    # Inicializar pusher

    App.addInitializer ->
        Pusher.channel_auth_endpoint = "#{Taller.host}#{Taller.api_endpoint}/pusher/auth?auth_token=#{Taller.auth_token}"

        App.pusher = new Pusher Taller.pusher_key

        App.channel = null

    # handlers

    App.reqres.setHandler "default:region", ->
        App.mainRegion

    App.reqres.setHandler "current:user", ->
        App.currentUser

    App.commands.setHandler "register:instance", (instance, id) ->
        App.register instance, id if App.environment is "development"

    App.commands.setHandler "unregister:instance", (instance, id) ->
        App.unregister instance, id if App.environment is "development"

    # Despues de inicializar la aplicaciones empezamos a ver los cambios en la url

    App.on "initialize:after", ->
        @startHistory root: "/", pushState: true
        @navigate(@rootRoute, trigger: true) unless @getCurrentRoute()

    App
