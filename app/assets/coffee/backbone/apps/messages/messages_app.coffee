define [
    "app"
    "bb/apps/messages/list/list_controller"
    "bb/apps/messages/list/list_view"
    "bb/entities/messages"
],

(App) ->

    App.module "MessagesApp", (MessagesApp, App, Backbone, Marionette, $, _) ->

        API =
            list: (region, workshop) ->
                new MessagesApp.List.Controller
                    region: region
                    workshop: workshop

        # handlers

        App.commands.setHandler "aside:region", (region, workshop) ->
            API.list region, workshop
