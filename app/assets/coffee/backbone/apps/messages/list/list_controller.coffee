define [
    "app"
    "bb/controllers/_base"
],

(App) ->

    App.module "MessagesApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.Controller extends App.Controllers.Base

            initialize: (options) ->
                { workshop } = options

                messages = App.request "message:entities", workshop

                messagesView = @gerMessagesView messages

                @listenTo messagesView, "new:comment", (message) ->
                    @newMessage message, messages, workshop

                @show messagesView, loading: on

            newMessage: (message, collection, workshop) ->
                me = App.request "current:user"
                model = App.request "new:message:entity"

                data =
                    author:
                        avatar: me.get "avatar"
                        id: me.get "id"
                        name: me.get "fullname"
                        # role: if me.get("role") is "admin" then "admin" else "guest"
                    workshop_id: workshop.get "id"
                    socket_id: App.pusher.connection.socket_id
                    text: message

                model.save data,
                    collection: collection

            # gets

            gerMessagesView: (messages) ->
                new List.Messages
                    collection: messages
