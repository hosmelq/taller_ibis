define [
    "app"
    "bb/views/_base/compositeview"
    "bb/views/_base/itemview"
    "jquery.elastic"
    "jquery.mutate"
],

(App) ->

    App.module "MessagesApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.Message extends App.Views.ItemView
            className: "chat__message"
            tagName: "li"
            template: "messages/list/message"

            serializeData: ->
                currentUser = App.request "current:user"

                if currentUser.get("id") is @model.get("author").id then @model.set current: on
                @model.toJSON()

        class List.Empty extends App.Views.ItemView
            className: "chat__message--empty"
            tagName: "li"
            template: "messages/list/empty"

        class List.Messages extends App.Views.CompositeView
            className: "chat"
            emptyView: List.Empty
            itemView: List.Message
            itemViewContainer: "ul"
            template: "messages/list/messages"

            ui:
                messages: ".chat__messages"
                sharer: ".chat__sharer-box"

            events:
                "keydown @ui.sharer": "processMessage"
                "click .chat__sharer-message": "focusTextarea"

            onShow: -> @ui.sharer.elastic()

            onDomRefresh: ->
                @monitorStream()
                @scrollToBottom()

            processMessage: (e) ->
                if e.keyCode is 13 and not e.shiftKey
                    e.preventDefault()
                    @trigger "new:comment", $.trim @ui.sharer.val()
                    @ui.sharer.val ""

            focusTextarea: -> @ui.sharer.focus()

            monitorStream: ->
                @ui.messages.mutate "scrollHeight", => @scrollToBottom()
                @ui.sharer.mutate "height", => @updateSize()

            scrollToBottom: ->
                height = @ui.messages.find(" > ul").height()
                @ui.messages.scrollTop height

            updateSize: _.throttle ->
                sharerHeight = @ui.sharer.outerHeight()
                messagesHeight = if sharerHeight > 20 then sharerHeight + 31 else 51

                @ui.messages.css "bottom", messagesHeight
                @scrollToBottom()
            , 10
