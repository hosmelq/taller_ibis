define [
    "app"
    "bb/apps/workshops/list/list_controller"
    "bb/apps/workshops/list/list_view"
    "bb/apps/workshops/show/show_controller"
    "bb/apps/workshops/show/show_view"
    "bb/entities/workshops"
],

(App) ->

    App.module "WorkshopsApp", (WorkshopsApp, App, Backbone, Marionette, $, _) ->

        class WorkshopsApp.Router extends Marionette.AppRouter
            appRoutes:
                "workshops/:id": "show"
                "workshops": "list"

        API =
            list: ->
                new WorkshopsApp.List.Controller

            show: (id, workshop) ->
                new WorkshopsApp.Show.Controller
                    id: id
                    workshop: workshop

        # inicialize

        App.addInitializer ->
            new WorkshopsApp.Router
                controller: API

        # events

        App.vent.on "workshop:clicked", (workshop) ->
            App.navigate "workshops/#{workshop.id}"
            API.show workshop.id, workshop
