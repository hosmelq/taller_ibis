define [
    "app"
    "bb/views/_base/layout"
    "bb/views/_base/compositeview"
    "bb/views/_base/itemview"
],

(App) ->

    App.module "WorkshopsApp.Show", (Show, App, Backbone, Marionette, $, _) ->

        class Show.Layout extends App.Views.Layout
            className: "grid main streaming"
            template: "workshops/show/layout"

            initialize: ->
                # @on "render", @updateLayout
                $(window).on "resize", @updateLayout

            regions:
                titleRegion: "#title-region"
                videoRegion: "#video-region"
                filesRegion: "#files-region"
                asideRegion: "#aside-region"

            updateLayout: ->
                videoRegion = $("#video-region")
                filesRegion = $('#files-region')
                contentHeight = videoRegion.closest(".grid-column-1-2").height()
                videoHeight = videoRegion.height()
                filesHeight = contentHeight - videoHeight - 40

                if filesHeight > 100
                    filesRegion.height filesHeight
                    filesRegion.show()
                else
                    filesRegion.hide()

        class Show.Title extends App.Views.ItemView
            className: "app__title-and-meta"
            template: "workshops/show/title"

        class Show.Video extends App.Views.ItemView
            className: "video"
            template: "workshops/show/video"

            triggers:
                "click .video-controls-fullscreen": "video:request:fullscreen"
