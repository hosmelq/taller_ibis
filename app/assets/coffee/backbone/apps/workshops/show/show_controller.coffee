define [
    "app"
    "peer_manager"
    "bb/controllers/_base"
    "bb/config/jquery/screenfull"
],

(App, PM) ->

    App.module "WorkshopsApp.Show", (Show, App, Backbone, Marionette, $, _) ->

        class Show.Controller extends App.Controllers.Base

            initialize: (options) ->
                { id, workshop } = options

                workshop or= App.request "workshop:entity", id

                App.execute "when:fetched", workshop, =>
                    @layout = @getLayoutView()

                    @listenTo @layout, "show", ->
                        @loadWorkshop workshop

                    @show @layout

            loadWorkshop: (workshop) ->
                currentUser = App.request "current:user"
                channelName = workshop.getChannelName()

                @subscribeChannel channelName

                session =
                    audio: off
                    oneway: on
                    screen: on

                window.PeerManager = new PM channelName,
                    session: session
                    userid: currentUser.id

                PeerManager.onstream = (event) ->
                    if event.type isnt "local"
                        attachMediaStream $('.video__video')[0], event.stream

                PeerManager.onNewSession = (session) ->
                    PeerManager.captureUserMedia ->
                        PeerManager.dontAttachStream = true
                        PeerManager.join session
                    , audio: on

                PeerManager.onclose = ->
                    PeerManager.leave()

                @startApps workshop

            startApps: (workshop) ->
                App.allUsers = App.request "set:all:users"

                # @titleView workshop
                @videoView workshop
                @filesRegion workshop
                @asideRegion workshop

                # if parseInt(workshop.get("user_id"), 10) is parseInt(currentUser.get("id"), 10)

            videoView: (workshop) ->
                videoView = @getVideoView workshop

                @listenTo videoView, "video:request:fullscreen", (args) ->
                    args.view.$el.find(".video__video").screenfull()

                @show videoView, region: @layout.videoRegion, loading: true

            titleView: (workshop) ->
                titleView = @getTitleView workshop

                @show titleView, region: @layout.titleRegion

            filesRegion: (workshop) ->
                App.execute "files:region", @layout.filesRegion, workshop

            asideRegion: (workshop) ->
                App.execute "aside:region", @layout.asideRegion, workshop

            subscribeChannel: (name) ->
                if typeof App.channel is "object" and App.channel isnt null then App.pusher.unsubscribe App.channel.name

                App.channel = App.pusher.subscribe name

            # gets

            getLayoutView: ->
                new Show.Layout

            getTitleView: (workshop) ->
                new Show.Title
                    model: workshop

            getVideoView: (workshop) ->
                new Show.Video
                    model: workshop
