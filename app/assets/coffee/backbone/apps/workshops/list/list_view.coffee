define [
    "app"
    "bb/views/_base/layout"
    "bb/views/_base/compositeview"
    "bb/views/_base/itemview"
],

(App) ->

    App.module "WorkshopsApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.Layout extends App.Views.Layout
            className: "grid main"
            template: "workshops/list/layout"

            regions:
                titleRegion: "#title-region"
                workshopsRegion: "#workshops-region"

        class List.Title extends App.Views.ItemView
            className: "app__title-and-meta"
            template: "workshops/list/title"

        class List.Workshop extends App.Views.ItemView
            tagName: "tr"
            template: "workshops/list/workshop"

            triggers:
                "click a": "workshop:clicked"

        class List.Empty extends App.Views.ItemView
            tagName: "tr"
            template: "workshops/list/empty"

        class List.Workshops extends App.Views.CompositeView
            className: "items-list"
            itemView: List.Workshop
            emptyView: List.Empty
            itemViewContainer: "tbody"
            template: "workshops/list/workshops"
