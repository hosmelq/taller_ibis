define [
    "app"
    "bb/controllers/_base"
],

(App) ->

    App.module "WorkshopsApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.Controller extends App.Controllers.Base

            initialize: ->
                workshops = App.request "workshop:entities"

                @layout = @getLayoutView()

                @listenTo @layout, "show", ->
                    @titleView()
                    @workshopsView workshops

                @show @layout

            titleView: ->
                titleView = @getTitleView()

                @show titleView, region: @layout.titleRegion

            workshopsView: (workshops) ->
                workshopsView = @getWorkshopsView workshops

                @listenTo workshopsView, "childview:workshop:clicked", (child, args) ->
                    App.vent.trigger "workshop:clicked", args.model

                @show workshopsView, region: @layout.workshopsRegion, loading: true

            # gets

            getLayoutView: ->
                new List.Layout

            getTitleView: ->
                new List.Title

            getWorkshopsView: (workshops) ->
                new List.Workshops
                    collection: workshops
