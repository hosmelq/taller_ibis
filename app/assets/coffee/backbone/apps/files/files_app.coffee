define [
    "app"
    "bb/apps/files/list/list_controller"
    "bb/apps/files/list/list_view"
    "bb/entities/files"
],

(App) ->

    App.module "FilesApp", (FilesApp, App, Backbone, Marionette, $, _) ->

        API =
            list: (region, workshop) ->
                new FilesApp.List.Controller
                    region: region
                    workshop: workshop

        # handlers

        App.commands.setHandler "files:region", (region, workshop) ->
            API.list region, workshop
