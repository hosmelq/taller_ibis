define [
    "app"
    "bb/views/_base/compositeview"
    "bb/views/_base/itemview"
],

(App, Dropzone) ->

    App.module "FilesApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.File extends App.Views.ItemView
            className: "files__file"
            tagName: "li"
            template: "files/list/file"

        class List.Empty extends App.Views.ItemView
            className: "files__file--empty"
            tagName: "li"
            template: "files/list/empty"

        class List.Files extends App.Views.CompositeView
            className: "files"
            emptyView: List.Empty
            itemView: List.File
            itemViewContainer: "ul"
            template: "files/list/files"
