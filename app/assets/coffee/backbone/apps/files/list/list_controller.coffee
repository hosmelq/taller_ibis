define [
    "app"
    "bb/controllers/_base"
],

(App) ->

    App.module "FilesApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.Controller extends App.Controllers.Base

            initialize: (options) ->
                { workshop } = options

                files = App.request "file:entities", workshop

                filesView = @getFilesView files, workshop

                @show filesView, loading: on

            # gets

            getFilesView: (files, workshop) ->
                new List.Files
                    collection: files
                    model: workshop
