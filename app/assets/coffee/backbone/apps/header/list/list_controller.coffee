define [
    "app"
    "bb/controllers/_base"
],

(App) ->

    App.module "HeaderApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.Controller extends App.Controllers.Base

            initialize: ->
                me = App.request "current:user"
                listView = @getListView me

                @show listView

            # gets

            getListView: (me) ->
                new List.Header
                    model: me
