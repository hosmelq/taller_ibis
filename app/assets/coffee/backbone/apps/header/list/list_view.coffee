define [
    "app"
    "bb/views/_base/itemview"
],

(App) ->

    App.module "HeaderApp.List", (List, App, Backbone, Marionette, $, _) ->

        class List.Header extends App.Views.ItemView
            className: "global-header"
            template: "header/list/header"
