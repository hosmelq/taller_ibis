define [
    "app"
    "bb/apps/header/list/list_controller"
    "bb/apps/header/list/list_view"
],

(App) ->

    App.module "HeaderApp", (HeaderApp, App, Backbone, Marionette, $, _) ->
        @startWithParent = false

        API =
            list: ->
                new HeaderApp.List.Controller
                    region: App.headerRegion

        HeaderApp.on "start", ->
            API.list()
