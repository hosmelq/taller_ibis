define [
    "app"
    "bb/entities/_base/models"
    "bb/entities/_base/collections"
],

(App) ->

    App.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

        class Entities.Workshop extends Entities.Model
            urlRoot: "/workshops"

            getChannelName: -> "presence-#{App.environment}-workshop-#{@get("id")}"

        class Entities.WorkshopsCollection extends Entities.Collection
            model: Entities.Workshop
            url: "/workshops"

        # API

        API =
            getWorkshops: ->
                workshops = new Entities.WorkshopsCollection
                workshops.fetch reset: on
                workshops

            getWorkshop: (id) ->
                workshop = new Entities.Workshop id: id
                workshop.fetch()
                workshop

        # handlers

        App.reqres.setHandler "workshop:entities", ->
            API.getWorkshops()

        App.reqres.setHandler "workshop:entity", (id) ->
            API.getWorkshop id
