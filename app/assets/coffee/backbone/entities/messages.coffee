define [
    "app"
    "bb/entities/_base/models"
    "bb/entities/_base/collections"
],

(App) ->

    App.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

        class Entities.Message extends Entities.Model
            urlRoot: -> "/messages"

        class Entities.MessagesCollection extends Entities.Collection
            model: Entities.Message
            url: -> "/workshops/#{@workshop.get("id")}/messages"

            initialize: (options) ->
                { @workshop } = options

                App.channel.bind "messages-update", @addMessage

            addMessage: (message) =>
                @add message

        # api

        API =

            getMessages: (workshop) ->
                messages = new Entities.MessagesCollection workshop: workshop
                messages.fetch reset: on
                messages

            newMessage: ->
                new Entities.Message

        # handlers

        App.reqres.setHandler "message:entities", (workshop) ->
            API.getMessages workshop

        App.reqres.setHandler "new:message:entity", ->
            API.newMessage()
