define [
    "app"
    "bb/entities/_base/models"
    "bb/entities/_base/collections"
],

(App) ->

    App.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

        class Entities.File extends Entities.Model
            urlRoot: -> "/files"

        class Entities.FilesCollection extends Entities.Collection
            model: Entities.File
            url: -> "/workshops/#{@workshop.get("id")}/files"

            initialize: (options) ->
                { @workshop } = options

                App.channel.bind "files-update", @addFile

            addFile: (file) =>
                @add file

        # api

        API =

            getFiles: (workshop) ->
                attachment = new Entities.FilesCollection workshop: workshop
                attachment.fetch reset: on
                attachment

            newFile: ->
                new Entities.File

        # handlers

        App.reqres.setHandler "file:entities", (workshop) ->
            API.getFiles workshop

        App.reqres.setHandler "file:entity", ->
            API.newFile()
