define [
    "app"
    "bb/entities/_base/models"
    "bb/entities/_base/collections"
    "webrtc.adapter"
],

(App) ->

    App.module 'Entities', (Entities, App, Backbone, Marionette, $, _) ->

        class Entities.User extends Entities.Model
            urlRoot: '/users'

            initialize: ->
                connection = @getConnection()

                # @listenTo connection, "addstream", @addStream
                # @listenTo connection, "removestream", @removeStream
                # @listenTo connection, "failed", @failed
                # @listenTo connection, "disconnected", @disconnected

            getConnection: (initiator = on) ->
                # PeerManager.get @id, initiator

            # addStream: (event) ->
            #     @set stream: event.stream, connecting: off
            #     @trigger "change:stream"

            # removeStream: -> @unset "stream", "connecting"

            # failed: -> @trigger "webrtc:failed"

            # disconnected: ->
            #     conversation = @get "conversation"
            #     conversation && conversation.participating && conversation.participating.remove @

        class Entities.UserMe extends Entities.User

            # initialize: ->
            #     @on "joinedroom", @joinedRoom, @

            getConnection: -> false

            joinedRoom: ->
                @requestUserMedia()

            requestUserMedia: ->
                return if @get "stream"

                @mediaConstraints =
                    audio: on
                    video:
                        mandatory:
                            maxFrameRate: 15
                            maxHeight: 360
                            maxWidth: 640

                audio =
                    optional: [{}]
                video =
                    mandatory:
                        maxFrameRate: 15
                        maxWidth: 320
                    optional: [{}]

                try
                    getUserMedia
                        audio: on
                        video: on
                    , @mediaSuccess, @mediaError
                catch error
                    @mediaError()

            mediaSuccess: (stream) =>
                @set stream: stream

            mediaError: (error) =>
                @trigger "webrtc:failed", error

        class Entities.UsersCollection extends Entities.Collection
            model: Entities.User
            # url: '/users'

            initialize: ->
                App.channel.bind "pusher:subscription_succeeded", @pusherJoined
                App.channel.bind "pusher:member_added", @pusherMemberAdded
                # App.channel.bind "pusher:member_removed", @pusherMemberRemoved

            pusherJoined: (members) =>
                currentUser = App.request "current:user"
                users = []
                users.push member for key, member of members.members when parseInt(key, 10) isnt parseInt(members.me.id, 10)
                users.unshift currentUser
                @reset users
                currentUser.trigger "joinedroom"

            pusherMemberAdded: (member) =>
                @add member.info

            pusherMemberRemoved: (member) =>
                model = @get member.id
                PeerManager.destroy member.id
                @remove model

        # API

        API =
            setCurrentUser: (currentUser) ->
                new Entities.UserMe currentUser

            setAllUsers: (users) ->
                new Entities.UsersCollection

        # handlers

        App.reqres.setHandler "set:current:user", (currentUser) ->
            API.setCurrentUser currentUser

        App.reqres.setHandler "set:all:users", ->
            API.setAllUsers()
