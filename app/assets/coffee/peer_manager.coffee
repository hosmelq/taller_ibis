define [
    "app"
    "rtcmulticonnection"
],

(App, RTCMultiConnection) ->

    class PeerManager extends RTCMultiConnection
        sessions: {}

        constructor: (channel, options = {}) ->
            super channel
            { @userid } = options

            _.defaults options, @_getConfig()

            _.assign @, options

            if @userid is 1 then @open() else @connect()

        _getConfig: ->
            autoCloseEntireSession: on
            mediaConstraints:
                mandatory:
                    minWidth: 640
                    maxWidth: 1280
                    minHeight: 360
                    maxHeight: 720
            session:
                audio: on
                fake: on
