define [
    'handlebars'
    'moment.es'
],

(Handlebars) ->

    # traducir texto

    Handlebars.registerHelper 'trans', (key) ->
        Taller.Locale?[key] or key

    # fecha con formato 'hace' y en español

    Handlebars.registerHelper 'moment', (date) ->
        moment(date).fromNow()

    # recortar texto

    Handlebars.registerHelper 'substr', (str, length) ->
        return str if not _.isNumber(length) or str.length < length

        "#{str.slice(0, length)}..."

    # verificar que una valor sea igual al dado

    Handlebars.registerHelper 'ifthis', (value, conditional, options) ->
        if value is conditional
            options.fn @
