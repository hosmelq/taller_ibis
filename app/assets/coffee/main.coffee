require.config
    baseUrl: "/js"
    deps: ["main"]
    paths:
        app: "backbone/app"
        bb: "backbone"
        backbone: "../bower_components/backbone/backbone"
        "backbone.babysitter": "../bower_components/backbone.babysitter/lib/amd/backbone.babysitter"
        marionette: "../bower_components/backbone.marionette/lib/core/amd/backbone.marionette"
        "backbone.syphon": "../bower_components/backbone.syphon/lib/amd/backbone.syphon"
        "backbone.wreqr": "../bower_components/backbone.wreqr/lib/amd/backbone.wreqr"
        handlebars: "../bower_components/handlebars/handlebars"
        jquery: "../bower_components/jquery/jquery"
        "jquery.elastic": "../bower_components/jquery-elastic/jquery.elastic.source"
        "jquery.mutate": "../bower_components/jquery-mutate/mutate"
        "jquery.mutate.events": "../bower_components/jquery-mutate/mutate.events"
        lodash: "../bower_components/lodash/dist/lodash"
        moment: "../bower_components/moment/moment"
        "moment.es": "../bower_components/moment/lang/es"
        pusher: "../bower_components/pusher/dist/pusher"
        spin: "../bower_components/spinjs/spin"
        "jquery.spin": "../bower_components/spinjs/jquery.spin"
        screenfull: "../bower_components/screenfull/dist/screenfull"
        socketio: "../bower_components/socket.io-client/dist/socket.io"
        "webrtc.adapter": "../bower_components/webrtc-adapter/adapter"
        rtcmulticonnection: "../bower_components/webrtc-experiment/RTCMultiConnection/Library/RTCMultiConnection-v1.5"

    shim:
        handlebars:
            init: ->
                @Handlebars = Handlebars
                @Handlebars

        backbone:
            deps: ["jquery", "underscore", "templates/templates", "templates/helpers"]
            exports: "Backbone"

        pusher: export: "Pusher"

        "jquery.elastic": ["jquery"]

        "jquery.mutate": ["jquery", "jquery.mutate.events"]

        rtcmulticonnection:
            exports: "RTCMultiConnection"

    map:
        "*":
            "underscore": "lodash"

require [
    'app'
    "bb/components/loading/main"
    "bb/apps/header/header_app"
    "bb/apps/workshops/workshops_app"
    "bb/apps/files/files_app"
    "bb/apps/messages/messages_app"
    "bb/entities/users"
],

(App) ->
    "use strict"

    window.App = App unless Taller.environment is "production"

    # Inicializar la alicaición

    App.start
        environment: Taller.environment
        currentUser: Taller.user
