define [
    "app"
    "backbone"
],

(App, Backbone) ->

    class PeerConnection
        msgQueue: []
        initiator: off
        started: off
        signalingReady: off
        localStream: null
        remoteStream: null
        options: {}

        constructor: (options = {}) ->
            # return console.log 'options:', options
            { @user_id, @peer_id, @initiator } = options
            @signalingReady = @initiator
            @options = options

            # ini
            @pc = @createPeerConnection()
            @pc.onicecandidate = @onIceCandidate
            @pc.onaddstream = @onRemoteStreamAdded
            @pc.onremovestream = @onRemoteStreamRemoved
            @pc.onsignalingstatechange = @onSignalingStateChanged
            @pc.oniceconnectionstatechange = @onIceConnectionStateChanged

            # App.channel.bind "client-message", @onChannelMessage
            App.channel.bind "pusher:subscription_succeeded", @channelReady

        _getConfig: ->
            mozilla:
                iceServers: [{
                    url: "stun:23.21.150.121"
                }]
            webkit:
                pcConstraints:
                    optional: [{
                        DtlsSrtpKeyAgreement: true
                    }, {
                        RtpDataChannels: true
                    }]
                scConstraints:
                    mandatory:
                        chromeMediaSource: "screen"
                sdpConstraints:
                    mandatory:
                        OfferToReceiveAudio: true
                        OfferToReceiveVideo: true
                offerConstraints:
                    optional: []
                    mandatory: {}

        channelReady: (members) =>
            @signalingReady = on
            if members.count > 1 then @initiator = on

        connect: (stream) ->
            @localStream = stream
            @msgQueue = []
            if not @pc then @constructor @options
            @maybeStart()

        maybeStart: ->
            console.log 'maybeStart:'
            console.log '@started:', @started
            console.log '@signalingReady:', @signalingReady
            console.log '@localStream:', @localStream
            if not @started and @signalingReady and @localStream
                # @createPeerConnection()
                @pc.addStream @localStream
                @started = on

                if @initiator then @doCall() else @calleeStart()

        createPeerConnection: ->
            console.log 'createPeerConnection:'

            if navigator.mozGetUserMedia
                config = _.defaults @options, @_getConfig().mozilla
            else if navigator.webkitGetUserMedia
                config = _.defaults @options, @_getConfig().webkit
            else
                return false

            new RTCPeerConnection iceServers: config.iceservers, config.constraints

        onIceCandidate: (event) =>
            console.log 'onIceCandidate:'
            if event.candidate
                # @sendMessage
                @msgQueue.push
                    candidate: event.candidate.candidate
                    id: event.candidate.sdpMid
                    from: @user_id
                    label: event.candidate.sdpMLineIndex

                @sendIceCandidates()
            else
                console.log 'End of candidates.'

        sendIceCandidates: _.throttle ->
            # @sendMessage @msgQueue
            App.channel.trigger "client-ice-#{@peer_id}", @msgQueue
            @msgQueue = []
        , 100

        onRemoteStreamAdded: (event) ->
            console.log('onRemoteStreamAdded')
            attachMediaStream($('.video__video')[0], event.stream)
            @remoteStream = event.stream

        onRemoteStreamRemoved: (event) ->
            console.log('onRemoteStreamRemoved.')

        onSignalingStateChanged: -> console.log 'onSignalingStateChanged:'

        onIceConnectionStateChanged: -> console.log 'onIceConnectionStateChanged:'

        calleeStart: ->
            console.log 'calleeStart:'
            while (@msgQueue.length > 0)
                @processSignalingMessage @msgQueue.shift()

        doCall: ->
            console.log 'doCall:'
            constraints = @mergeConstraints @options.offerConstraints, @options.sdpConstraints
            @pc.createOffer @setLocalAndSendMessage, null, constraints

        doAnswer: ->
            console.log 'doAnswer:'
            @pc.createAnswer @setLocalAndSendMessage, null, @options.sdpConstraints

        setLocalAndSendMessage: (sessionDescription) =>
            console.log 'setLocalAndSendMessage:'
            sessionDescription.sdp = s.preferOpus sessionDescription.sdp
            @pc.setLocalDescription sessionDescription
            @sendMessage sessionDescription

        mergeConstraints: (cons1, cons2) ->
            console.log 'mergeConstraints:'
            merged = cons1
            for name of cons2.mandatory
                merged.mandatory[name] = cons2.mandatory[name]
                merged.optional.concat cons2.optional

            merged

        setRemote: (message) ->
            console.log 'setRemote:'
            @pc.setRemoteDescription new RTCSessionDescription(message)

        sendMessage: (message) ->
            console.log 'sendMessage:'
            data =
                type: message.type
                from: @user_id
            data[message.type] = message

            App.channel.trigger "client-#{message.type}-#{@peer_id}", data
            # App.channel.trigger "client-message", data

        onChannelMessage: (message) =>
            console.warn 'onChannelMessage:'
            console.log '@initiator:', @initiator
            console.log '@started:', @started
            # msg = JSON.parse message.data
            msg = message
            if not @initiator and not @started
                if msg.type is "offer"
                    @msgQueue.unshift msg
                    signalingReady = on
                    @maybeStart()
                else
                    @msgQueue.push msg
            else
                console.log 'after processSignalingMessage:'
                @processSignalingMessage msg

        processSignalingMessage: (message) ->
            console.log 'processSignalingMessage:'
            return if not @started

            if message.type is "offer"
                @pc.setRemoteDescription new RTCSessionDescription message.offer
                # @setRemote message
                @doAnswer()

            else if message.type is "answer"
                # @setRemote message
                @pc.setRemoteDescription new RTCSessionDescription message.answer

            else if _.isArray message
                for key, value of message
                    candidate = new RTCIceCandidate
                        sdpMLineIndex: value.label
                        candidate: value.candidate

                    @pc.addIceCandidate candidate if candidate.candidate isnt "undefined"

            else if message.type is "bye"
                onRemoteHangup()

        onRemoteHangup: ->
            console.log 'onRemoteHangup:'
            @initiator = 0
            @stop()

        stop: ->
            console.log 'stop:'
            @started = off
            @signalingReady = off
            @pc.close()
            @pc = null
            @msgQueue.length = 0

        disconnect: ->
            # clearTimeout(@timeoutFailed)
            # clearTimeout(@timeoutPause)
            @started = off
            @signalingReady = off
            # @paused = off
            if @pc and @localStream
                @pc.removeStream @localStream
                @localStream = null
                @pc.close()
                @pc = null

s =
  preferOpus: (e) ->
    t = e.split("\r\n")
    i = 0

    while i < t.length
      if -1 isnt t[i].search("m=audio")
        n = i
        break
      i++
    return e  if null is n
    i = 0

    while i < t.length
      if -1 isnt t[i].search("opus/48000")
        s = @extractSdp(t[i], /:(\d+) opus\/48000/i)
        if s
          t[n] = @setDefaultCodec(t[n], s)
          break
      i++
    t = @removeCN(t, n)
    e = t.join("\r\n")

  addStereo: (e) ->
    t = e.split("\r\n")
    i = 0

    while i < t.length
      if -1 isnt t[i].search("opus/48000")
        n = @extractSdp(t[i], /:(\d+) opus\/48000/i)
        break
      i++
    i = 0

    while i < t.length
      if -1 isnt t[i].search("a=fmtp")
        s = @extractSdp(t[i], /a=fmtp:(\d+)/)
        if s is n
          r = i
          break
      i++
    (if null is r then e else (t[r] = t[r].concat(" stereo=1")
    e = t.join("\r\n")
    ))

  extractSdp: (e, t) ->
    i = e.match(t)
    (if i and 2 is i.length then i[1] else null)

  setDefaultCodec: (e, t) ->
    i = e.split(" ")
    n = new Array
    s = 0
    r = 0

    while r < i.length
      3 is s and (n[s++] = t)
      i[r] isnt t and (n[s++] = i[r])
      r++
    n.join " "

  setInactive: (e) ->
    e.replace /a=recvonly/g, "a=inactive"

  removeCN: (e, t) ->
    i = e[t].split(" ")
    n = e.length - 1

    while n >= 0
      s = @extractSdp(e[n], /a=rtpmap:(\d+) CN\/\d+/i)
      if s
        r = i.indexOf(s)
        -1 isnt r and i.splice(r, 1)
        e.splice(n, 1)
      n--
    e[t] = i.join(" ")
    e
