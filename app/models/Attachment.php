<?php

class Attachment extends BaseModel {
    protected $table = 'attachments';

	protected $guarded = [];

    protected $hidden = ['path'];
}
