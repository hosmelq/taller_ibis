<?php

class BaseModel extends Eloquent {

    /**
     * When create a model, run the attributes
     * through a validator first.
     *
     * @param array $attributes
     *
     * @return void
     */
    // public static function create(array $attributes = [])
    // {
    //     $class = get_class($this);
    //     $path = "Acme\\Services\\Validation\\{$class}Validator";

    //     if (class_exists($path)) {
    //         // App::make($path)->validateForCreation($attributes);
    //     }

    //     parent::create($attributes);
    // }

    /**
     * When updating a model, run the attributes
     * through a validator first.
     *
     * @param array $attributes
     *
     * @return void
     */
    public function update(array $attributes = [])
    {
        $class = get_class($this);
        $path = "Acme\\Services\\Validation\\{$class}Validator";

        if (class_exists($path)) {
            App::make($path)->validateForUpdate($attributes);
        }

        parent::update($attributes);
    }
}
