<?php

class Message extends Moloquent {
    protected $connection = 'mongodb';

    protected $collection = 'messages';

    protected $fillable = ['author', 'workshop_id', 'text'];
}
