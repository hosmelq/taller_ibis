<?php

class Workshop extends BaseModel {
    protected $table = 'workshops';

	protected $guarded = array();

    /**
     * user creator
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * Converte attribute status to int
     *
     * @return int
     */
    public function getStatusAttribute($value)
    {
        return (int) $value;
    }
}
