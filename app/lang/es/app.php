<?php

return array(

    # login
    'login_sign_in'           => 'Iniciar sesión',
    'login_email'           => 'Correo Electrónico',
    'login_password'        => 'Contraseña',
    'login_required_fileds' => 'Correo Electrónico o contraseña incorrectos.',

    # nav
    'nav_greeting'          => 'Bienvenido',
    'nav_sign_out'          => 'Salir',
    'nav_courses'           => 'Cursos',

    #apps
    'app_list'              => 'Lista',

    # apps titles
    'app_courses'           => 'Cursos',

    # apps items
    'app_title'             => 'Título',
    'app_speaker'           => 'Ponente',
    'app_location'          => 'Ubicación',
    'app_status'            => 'Estado',
    'app_active'            => 'Activo',
    'app_inactive'          => 'Inactivo',

);
