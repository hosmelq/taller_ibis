<?php

return array(

    # login
    'login_sign_in'         => 'Sign in',
    'login_email'           => 'Email Address',
    'login_password'        => 'Password',
    'login_required_fileds' => 'Email Address or Password incorrect.',

    # nav
    'nav_greeting'          => 'Hey',
    'nav_sign_out'          => 'Sign Out',
    'nav_courses'           => 'Courses',

    #apps
    'app_list'              => 'List',

    # apps titles
    'app_courses'           => 'Courses',

    # apps items
    'app_title'             => 'Title',
    'app_speaker'           => 'Speaker',
    'app_location'          => 'Location',
    'app_status'            => 'Status',
    'app_active'            => 'Active',
    'app_inactive'          => 'Inactive',

);
